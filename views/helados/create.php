<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Helados */

$this->title = 'Crear Helado';
$this->params['breadcrumbs'][] = ['label' => 'Helados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="helados-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
