<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "helados".
 *
 * @property int $id
 * @property string $nombre
 * @property string $sabor
 * @property int $tamano
 */
class Helados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'helados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tamano'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['sabor'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Referencia',
            'nombre' => 'Nombre',
            'sabor' => 'Sabor',
            'tamano' => 'Tamaño',
        ];
    }
}
