﻿DROP DATABASE IF EXISTS helados;
CREATE DATABASE helados;

USE helados;

CREATE OR REPLACE TABLE helados(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  sabor varchar(20),
  tamano int,
  PRIMARY KEY (id)
);